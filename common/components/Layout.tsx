import NavBar from "./NavBar";
import Footer from "./Footer";

const Layout = ({ children }) => {
  return (
    <>
      <NavBar />
      <div>
        {children}
        <Footer />
      </div>
    </>
  );
};

export default Layout;
