import Head from "next/head";

import { withTranslation } from "../../i18n";

const AppHead = ({ t }) => (
  <Head>
    <title>{t("Title")}</title>
    <link rel="icon" href="/favicon.ico" />
  </Head>
);

export default withTranslation("common")(AppHead);
