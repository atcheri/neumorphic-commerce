import Image from "next/image";
import Link from "next/link";

import { i18n, withTranslation } from "../../i18n";

const NavBar = ({ t }) => {
  return (
    <nav>
      <Image
        className="logo"
        src="/vercel.svg"
        alt="Logo"
        width={160}
        height={80}
      />
      <Link href="/">
        <a>{t("Homepage")}</a>
      </Link>
      <button
        type="button"
        onClick={() =>
          i18n.changeLanguage(i18n.language === "en" ? "fr" : "en")
        }
      >
        {t("Change-locale")}
      </button>
    </nav>
  );
};

export default withTranslation("navbar")(NavBar);
