import { withTranslation } from "../i18n";

const Home = ({ t }) => {
  return (
    <main>
      <h1>Welcome to the Neumorphic Ecommerce</h1>
    </main>
  );
};

Home.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation("common")(Home);
