import App from "next/app";
import AppHead from "../common/components/AppHead";
import Layout from "../common/components/Layout";
import { appWithTranslation } from "../i18n";

import "../styles/globals.scss";

const MyApp = ({ Component, pageProps }) => (
  <Layout>
    <AppHead />
    <Component {...pageProps} />
  </Layout>
);

MyApp.getInitialProps = async (appContext) => ({
  ...(await App.getInitialProps(appContext)),
});

export default appWithTranslation(MyApp);
